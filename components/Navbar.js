import Link from 'next/link';
import { useContext } from 'react';
import { useRouter } from 'next/router';
import AppContext from '../context/StateContext';

const Navbar = () => {
    const { token, setToken } = useContext(AppContext);
    const router = useRouter();

    function handleOut() {
        localStorage.removeItem("token");
        setToken(null);
        router.push("/");
    }

    if(token==null)
    {
        return (
            <nav className="navbar">
                <Link href="/">
                    <a className="navbar-brand">Blog App</a>
                </Link>
                <Link href="/login">
                    <a className="create">Login</a>
                </Link>
                <Link href="/signup">
                    <a className="create">Signup</a>
                </Link>
            </nav>
        )
    }

    return (
    <nav className="navbar">
        <Link href="/">
            <a className="navbar-brand">Blog App</a>
        </Link>
        <Link href="/new">
            <a className="create">Create note</a>
        </Link>
        <Link href="#">
            <a className="create" onClick={handleOut}>Logout</a>
        </Link>
    </nav>
)
    }
export default Navbar;