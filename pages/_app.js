// import App from 'next/app'
import 'semantic-ui-css/semantic.min.css'
import Layout from '../components/Layout';
import '../css/style.css';
import AppContext from '../context/StateContext';
import { useEffect, useState } from 'react';

function MyApp({ Component, pageProps }) {

    const [token, setToken] = useState(null);

    useEffect(()=>{
        setToken(localStorage.getItem("token"));
    }, [])

    return (
    <AppContext.Provider value={{token, setToken}}>
        <Layout>
            <Component {...pageProps} />
        </Layout>
    </AppContext.Provider>
    )
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp