import Link from 'next/link';
import { useRouter } from 'next/router';
import fetch from 'isomorphic-unfetch';
import { Button, Card } from 'semantic-ui-react';
import { useContext, useState } from 'react';
import { useEffect } from 'react';
import Axios from 'axios';
import AppContext from '../context/StateContext';
import * as Constants from '../constants';

const Index = () => {
  const [notes, setNotes] = useState([]);
  const router = useRouter();

  useEffect(()=>{
    const token = localStorage.getItem("token");
    async function fetchData() {
      try
      {
        const res = await Axios.get(`${Constants.BACKEND_URI}/items`, {
                                    headers: {Authorization: `Bearer ${token}`
                                        }
                                    }
                                  );
        setNotes(res.data);
      } catch(error) {
        console.log(error);
      }
    }
    fetchData();
  }, []);

  return (
    <div className="notes-container">
      <h1>Blogs</h1>
      <div className="grid wrapper">
        {notes.map(note => {
          return (
            <div key={note.id}>
              <Card>
                <Card.Content>
                  <Card.Header>
                    <Link href={`/${note.id}`}>
                      <a>{note.title}</a>
                    </Link>
                  </Card.Header>
                </Card.Content>
                <Card.Content extra>
                  <Link href={`notes?id=${note.id}`}>
                    <Button primary>View</Button>
                  </Link>
                  <Link href={`notes/edit?id=${note.id}`}>
                    <Button primary>Edit</Button>
                  </Link>
                </Card.Content>
              </Card>
            </div>
          )
        })}
      </div>
    </div>
  )
}

// Index.getInitialProps = async () => {
//   const res = await fetch('https://random-data-api.com/api/coffee/random_coffee');
//   // const { data } = [
//   //   {id: 1, title: "fhgdhg", description: "gjhfjkjkjkjk" },
//   //   {id: 2, title: "fhgdhg", description: "gjhfjkjkjkjk" },
//   //   {id: 3, title: "fhgdhg", description: "gjhfjkjkjkjk" },
//   //   {id: 4, title: "fhgdhg", description: "gjhfjkjkjkjk" },
//   //   {id: 5, title: "fhgdhg", description: "gjhfjkjkjkjk" }
//   // ];
//   console.log(res)

export default Index;