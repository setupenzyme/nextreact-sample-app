import Link from 'next/link';
import { useState, useEffect } from 'react';
import fetch from 'isomorphic-unfetch';
import { Button, Form, Loader } from 'semantic-ui-react';
import { useRouter } from 'next/router';
import Axios from 'axios';
import * as Constants from '../../constants';

const EditNote = ({ prop }) => {
    const [form, setForm] = useState({ title: "", description: "" });
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [errors, setErrors] = useState({});
    const router = useRouter();

    useEffect(() => {
        if (isSubmitting) {
            if (Object.keys(errors).length === 0) {
                updateNote();
            }
            else {
                setIsSubmitting(false);
            }
        }
    }, [errors])

    useEffect(()=>{
        async function fetchData() {
            const token = localStorage.getItem("token");
            const noteId = router.query.id;
            console.log(noteId, "gfvhgfvg");
            try
                {
                    const res = await Axios.get(`${Constants.BACKEND_URI}/items/${noteId}`, {
                                                headers: {Authorization: `Bearer ${token}`
                                                    }
                                                }
                                                );
                    setForm(res.data);
                    } catch(error) {
                    console.log(error.response.data);
                    }
        }
        fetchData();
    }, [])

    const updateNote = async () => {
        const token = localStorage.getItem("token");
        const noteId = router.query.id;
        try {
            const res = await Axios.put(`${Constants.BACKEND_URI}/items/${noteId}`,
            {title: form.title, description: form.description}, 
            {headers: 
                            {Authorization: `Bearer ${token}`}});
            router.push("/");
        } catch (error) {
            console.log(error.response.data);
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let errs = validate();
        setErrors(errs);
        setIsSubmitting(true);
    }

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const validate = () => {
        let err = {};

        if (!form.title) {
            err.title = 'Title is required';
        }
        if (!form.description) {
            err.description = 'Description is required';
        }

        return err;
    }

    return (
        <div className="form-container">
            <h1>Update Note</h1>
            <div>
                {
                    isSubmitting
                        ? <Loader active inline='centered' />
                        : <Form onSubmit={handleSubmit}>
                            <Form.Input
                                fluid
                                error={errors.title ? { content: 'Please enter a title', pointing: 'below' } : null}
                                label='Title'
                                placeholder='Title'
                                name='title'
                                value={form.title}
                                onChange={handleChange}
                            />
                            <Form.TextArea
                                fluid
                                label='Descriprtion'
                                placeholder='Description'
                                name='description'
                                error={errors.description ? { content: 'Please enter a description', pointing: 'below' } : null}
                                value={form.description}
                                onChange={handleChange}
                            />
                            <Button type='submit'>Update</Button>
                        </Form>
                }
            </div>
        </div>
    )
}

EditNote.getInitialProps = async ({ query: { id } }) => {
    // const res = await fetch(`http://localhost:3000/api/notes/${id}`);
    // const { data } = await res.json();

    return { prop: id }
}

export default EditNote;
