import fetch from 'isomorphic-unfetch';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { Confirm, Button, Loader } from 'semantic-ui-react';
import { useContext } from "react";
import AppContext from '../../context/StateContext';
import Axios from 'axios';
import * as Constants from '../../constants';

const Note = ({ prop }) => {
    const value = useContext(AppContext);
    const [confirm, setConfirm] = useState(false);
    const [isDeleting, setIsDeleting] = useState(false);
    const [token, setToken] = useState(null);
    const router = useRouter();
    const [note, setNote] = useState({});

    console.log(value);

    useEffect(() => {
        if (isDeleting) {
            deleteNote();
        }
    }, [isDeleting])

    useEffect(()=>{
        const token = localStorage.getItem("token");
        const noteId = router.query.id;
        async function fetchData() {
            try
            {
              const res = await Axios.get(`${Constants.BACKEND_URI}/items/${noteId}`, {
                                          headers: {Authorization: `Bearer ${token}`
                                              }
                                          }
                                        );
              setNote(res.data);
            } catch(error) {
              console.log(error);
            }
          }
          fetchData();
    }, [])

    const open = () => setConfirm(true);

    const close = () => setConfirm(false);

    const deleteNote = async () => {
        const token = localStorage.getItem("token");
        const noteId = router.query.id;
        try {
            const deleted = await Axios.delete(`${Constants.BACKEND_URI}/items/${noteId}`, {
                headers: {Authorization: `Bearer ${token}`
                    }
                });

            router.push("/");
        } catch (error) {
            console.log(error)
        }
    }

    const handleDelete = async () => {
        setIsDeleting(true);
        close();
    }

    return (
        <div className="note-container">
            {isDeleting
                ? <Loader active />
                :
                <>
                    <h1>{note.title}</h1>
                    <p>{note.description}</p>
                    <Button color='red' onClick={open}>Delete</Button>
                </>
            }
            <Confirm
                open={confirm}
                onCancel={close}
                onConfirm={handleDelete}
            />
        </div>
    )
}

Note.getInitialProps = async ({ query: { id } }) => {
    // const res = await fetch(`http://localhost:3000/api/notes/${id}`);
    // const { data } = await res.json();

    return { prop: id }
}

export default Note;
