import Link from 'next/link';
import { useState, useEffect, useContext } from 'react';
import fetch from 'isomorphic-unfetch';
import { Button, Form, Loader } from 'semantic-ui-react';
import { useRouter } from 'next/router';
import Axios from "axios";
import AppContext from '../context/StateContext';
import * as Constants from '../constants';

const SignUp = () => {
    const [form, setForm] = useState({ email: "", name: "", password: "" });
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [errors, setErrors] = useState({});
    const { token, setToken } = useContext(AppContext);
    const router = useRouter();

    useEffect(() => {
        if (isSubmitting) {
            if (Object.keys(errors).length === 0) {
                sendCreds();
            }
            else {
                setIsSubmitting(false);
            }
        }
    }, [errors])

    const sendCreds = async () => {
        try {
            const formData = new FormData();
            formData.append('username', form.email);
            formData.append('password', form.password);
            const res = await Axios.post(`${Constants.BACKEND_URI}/users/open`, {username: form.email, ...form});
            const res2 = await Axios.post(`${Constants.BACKEND_URI}/login/access-token`, formData);
            localStorage.setItem("token", res2.data.access_token);
            setToken(res2.data.access_token);
            router.push("/");
        } catch (error) {
            console.log(error);
            try {
                const res3 = await Axios.post(`${Constants.BACKEND_URI}/login/access-token`, formData);
            } catch(error) {
                console.log(error);
            }
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let errs = validate();
        setErrors(errs);
        setIsSubmitting(true);
    }

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }

    const validate = () => {
        let err = {};

        if (!form.email) {
            err.email = 'email is required';
        }
        if (!form.name) {
            err.name = 'name is required';
        }
        if (!form.password) {
            err.password = 'password is required';
        }

        return err;
    }

    return (
        <div className="form-container">
            <h1>Log In</h1>
            <div>
                {
                    isSubmitting
                        ? <Loader active inline='centered' />
                        : <Form onSubmit={handleSubmit}>
                            <Form.Input
                                fluid
                                error={errors.email ? { content: 'Please enter a email', pointing: 'below' } : null}
                                label='Email'
                                placeholder='Email'
                                name='email'
                                value={form.email}
                                onChange={handleChange}
                            />
                            <Form.Input
                                fluid
                                error={errors.name ? { content: 'Please enter a name', pointing: 'below' } : null}
                                label='Name'
                                placeholder='Name'
                                name='name'
                                value={form.name}
                                onChange={handleChange}
                            />
                            <Form.Input
                                fluid
                                label='Password'
                                placeholder='Password'
                                name='password'
                                error={errors.password ? { content: 'Please enter a password', pointing: 'below' } : null}
                                value={form.password}
                                onChange={handleChange}
                            />
                            <Button type='submit'>Log In</Button>
                        </Form>
                }
            </div>
        </div>
    )
}

export default SignUp;
